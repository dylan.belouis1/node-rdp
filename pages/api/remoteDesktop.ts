import rdp from "../../node-rdp";
import { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method !== "POST") {
    return res.status(405).end();
  }

  const { address, username, password } = req.body;

  if (!address || !username || !password) {
    return res.status(400).json({ error: "Missing required parameters." });
  }

  try {
    await rdp({
      address: `${address}:3389`,
      username: username,
      password: password,
      enableDrives: "*",
    });

    // If the connection is successful, you can send a success response.
    res.status(200).json({ message: "Connection successful." });
  } catch (error) {
    console.error("RDP Connection Error:", error);
    res.status(500).json({
      error: "An error occurred while establishing the RDP connection.",
    });
  }
}
