import { useState } from "react";

export default function RemoteDesktopPage() {
  const [address, setAddress] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [responseMessage, setResponseMessage] = useState("");

  const handleConnect = async () => {
    try {
      const response = await fetch("/api/remoteDesktop", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          address: address,
          username: username,
          password: password,
        }),
      });

      if (response.ok) {
        setResponseMessage("Connection successful.");
      } else {
        const data = await response.json();
        setResponseMessage(data.error || "An error occurred.");
      }
    } catch (error) {
      setResponseMessage("An error occurred while connecting.");
    }
  };

  return (
    <div>
      <input
        type="text"
        placeholder="Address"
        value={address}
        onChange={(e) => setAddress(e.target.value)}
      />
      <input
        type="text"
        placeholder="Username"
        value={username}
        onChange={(e) => setUsername(e.target.value)}
      />
      <input
        type="password"
        placeholder="Password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <button onClick={handleConnect}>Connect</button>
      <div>{responseMessage}</div>
    </div>
  );
}
